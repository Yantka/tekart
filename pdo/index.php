<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Новости</title>
    <link href="../style.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="news-block">
            <div class="news-block__title news-block__title--block-title"><h1>Новости</h1></div>
                <div class="news-block__news-items">
                
                    <?php 
                        include_once "connection.php"; //Подключение БД
                        include_once "functions.php";  //Подключение функций
                    
                    // Переменная хранит число сообщений выводимых на станице
                    $news_per_page = 5;

                    // Получаем текущую страницу из URL
                    if(isset($_GET['page']) && intval($_GET['page']) > 0){
                        $page = intval($_GET['page']);
                    }else{
                        $page = 1;
                    }

                    // Определяем общее число сообщений в базе данных
                    $result = $pdo->query('SELECT * FROM `news`');
                    $posts = $result->fetchAll(PDO::FETCH_ASSOC);
                    

                    // Находим общее число страниц
                    $count_pages = count($posts);
                    $total = intdiv($count_pages, $news_per_page)+1;

                    // Если значение $page меньше единицы или отрицательно
                    // переходим на первую страницу
                    // А если слишком большое, то переходим на последнюю
                    if(empty($page) or $page < 0) $page = 1;
                    if($page > $total) $page = $total;

                    // Вычисляем начиная к какого номера
                    // следует выводить сообщения
                    $start = $page * $news_per_page - $news_per_page;

                    // Выбираем $num сообщений начиная с номера $start
                    $sql = "select * from `news` ORDER BY `idate` DESC LIMIT $start , $news_per_page";
                    
                    $result = $pdo->query($sql);
                    $rows = $result->fetchAll(PDO::FETCH_ASSOC);
                    

                    for($i = 0; $i < $news_per_page; $i++){
                        echo showNews($rows[$i]);
                    }
                    ?>
                
            </div>
            <div class="pagination-block">
                <div class="pagination-block__title news-block__title--block-title"><h2>Страницы:</h2></div>
                <div class="pagination-block__pages">
                    <?php
                    for($n = 1; $n < ($total+1); $n++){
                        if(isset($_GET['page']) && $_GET['page'] == $n ){
                            echo ' <a class="active" href= /pdo/?page='. $n .'><span>'. $n .'</span></a>';
                        }elseif($n == 1 && !isset($_GET['page'])){
                            echo ' <a class="active" href= /pdo/?page='. $n .'><span>'. $n .'</span></a>';
                        }else{
                            echo ' <a href= /pdo/?page='. $n .'><span>'. $n .'</span></a>';
                        }
                    }
                    ?>
                </div>
            </div>
            
        </div>
    </div>
</body>
</html>