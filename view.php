<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Новость</title>
    <link href="style.css" rel="stylesheet">
</head>
<body>
    <?php
        include_once "connection.php"; //Подключение БД
        
        if(isset($_GET['id']) && intval($_GET['id']) > 0){
            $item = intval($_GET['id']);
        }
        $sql = "select * from `news` WHERE `id` = $item";
        $result = $mysql->query($sql); if (!$result) die($mysql->error);
        $post = $result->fetch_all(MYSQLI_ASSOC);

        
    ?>
    <div class="container">
        <div class="news-block">
            <div class="news-block__title news-block__title--detail"><h1><?=$post[0]['title']?></h1></div>
                <div class="news-item__item-content"><?=$post[0]['content']?></div>
            <div class="pagination-block pagination-block--detail">
                <div class="pagination-block__title pagination-block__title--detail-title"><h3><a href="/news.php?page=1">Все новости > ></a></h3></div>
            </div>
        </div>
    </div>

</body>
</html>
