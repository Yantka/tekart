<?php
function showNews($data) {
    $result = '';
    if(is_array($data)){
        foreach($data as $k => $row){
            $result .='<div class="news-block__news-item">';
                        $result .='<div class="news-block__news-item__firest-row">
                            <div>'.date('d.m.Y',$row['idate']).'</div>
                            <div><a href="/view.php?id='.$row['id'].'">'.$row['title'].'</a></div>
                        </div>';
                        $result .= '
                            <div class="news-block__news-item__second-row">
                                <div>'.$row['announce'].'</div>
                            </div>
                        ';
            $result .= '</div>';
        }
    }
    return $result;
}